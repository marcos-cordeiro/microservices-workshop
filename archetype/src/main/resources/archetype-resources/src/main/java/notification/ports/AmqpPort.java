package ${package}.notification.ports;

import ${package}.commons.errors.domains.DefaultErrorResponse;

public interface AmqpPort {

  void notifyNotificationOperationError(DefaultErrorResponse errorResponse);

}
